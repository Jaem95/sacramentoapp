﻿var express = require("express");
var app = express();
var path = require("path");
const fs = require('fs');
const systemRouter = require('./system_router');
const DatabaseConnector = require('./lib/DatabaseController');
var mysql = require('mysql');
var years = [];
var total = [];

const bodyParser = require('body-parser');
const cookies = require('cookie');
const cookieParser = require('cookie-parser');
const pdf = require('html-pdf');

var parser= bodyParser.urlencoded({extended:false});

app.use(express.static(path.join(__dirname, 'pages')));
app.use(cookieParser());

app.get('/', (req,res)=>{
	res.sendFile(path.join(__dirname,"pages/index.html"));
});

app.get('/plusParish', (req,res)=>{
	res.sendFile(path.join(__dirname,"pages/image.html"));
});

app.post('/try_login',parser,(req,res)=>{
	var user=req.body.user;
	var pass=req.body.pass;

	var conn = DatabaseConnector.getDatabaseConnection();
	var argument_query="select p1.id, p1.nombre, p1.apellidoP,p1.apellidoM, p1.parroquia idParroquia,"+
	"pa.nombre parroquia,t.nombre titulo, p2.id pid, p2.nombre pnom from personal p1 "+ 
	"inner join parroquia pa on pa.id=p1.parroquia "+
	 "AND p1.correo='"+user+"' AND p1.contrasena='"+pass+"'"+
	 " inner join personal p2 on pa.responsable=p2.id"+
	 " inner join tipoDeTitulos t on p1.titulo=t.id";
	conn.select('personal',argument_query,(err,doc)=>{
			console.log("error: "+err);
			if(typeof doc !== 'undefined'){
				if(doc[0] == null){
					//cursor.close();
					conn.close();
					res.send('0');
				}else{
					console.log("****");
					console.log("user in database: "+doc[0].id+"//"+doc[0].correo);
					//cursor.close();
					for(var name in doc[0]){
						var value= doc[0][name];
						console.log("{"+name+" : "+value+"}");
					}
					//console.log("id: "+doc[0].pid+" nom: "+doc[0].pnom);
					var json_user = doc[0].id +"-**-"+
											doc[0].nombre +"-**-"+
											doc[0].apellidoP +"-**-"+
											doc[0].apellidoM +"-**-"+
											doc[0].idParroquia +"-**-"+
											doc[0].parroquia +"-**-"+
											doc[0].titulo +"-**-"+
											doc[0].pid +"-**-"+
											doc[0].pnom;
					
					var cookie_user= new UserDp(json_user);
					//console.log(json_user);
					console.log(cookie_user.toString());
					//console.log(cookie_user.toStringNoSpace());
					res.cookie('user', cookie_user.toString(), { maxAge: 30*60*1000 });
					conn.close();
					//res.redirect('./administrar');
					res.send('1');
					
				}
			}else{
				res.send('0');
			}
	});
	console.log("Trying to access with user: "+user+" pass: "+pass);
	
});

app.post('/insertSacramento',myMiddleware,parser,(req,res)=>{
	var u_cookie=req.cookies.user;
	var str_info=req.body.info;
	var scramento=req.body.sacramento;
	var argument_query="SET ?";
	var table="";

	//console.log("str_info: "+str_info);
	
	var json_info = JSON.parse(str_info);
	//console.log("json_info: "+json_info);
	var us= new UserDp(u_cookie);

	json_info.parroquia=us.getIdParroquia();
	json_info.registradoPor=us.getId();

	if(scramento==1){
		table="bautizos";

	}else if (scramento==2) {
		table="primera_comunion";
	}else if (scramento== 3) {
		table="confirmacion";
	}

	var conn = DatabaseConnector.getDatabaseConnection();

	//(nombre) VALUES('Toluca')
	
	conn.insert(table,argument_query,json_info,(err,doc)=>{
			console.log("err: "+err);
			if(err==null){
				if(typeof doc !== 'undefined'){
					if(doc == null){
						//cursor.close();
						conn.close();
						res.send('0');
					}else{
						
						conn.close();
						//res.redirect('./administrar');
						res.send('1');
						
					}
				}else{
					conn.close();
					res.send('0');
				}
			}else{
				if (err.code=="ER_TRUNCATED_WRONG_VALUE") {
					conn.close();
					res.send('3');
				}else{
					conn.close();
					res.send('0');
				}	
			}
	});

});

app.get('/administrar',myMiddleware, (req,res)=>{
	res.sendFile(path.join(__dirname,"pages/start.html"));
});
app.get('/editarIniciacion',myMiddleware, (req,res)=>{
	res.sendFile(path.join(__dirname,"pages/edit.html"));
});


app.post('/generateGraphic', myMiddleware, parser,(req, res) => {
    var sacramentos = req.body.sacramen;
    var anio = req.body.anio;
    var periodo = req.body.periodo;
    var month = req.body.mes;
    console.log(month);

    var table = "";
    var fecha = "";
 

    switch (sacramentos) {
        case "1":
            table = "bautizos";
            fecha = "fechaBautizo";
            break;
        case "2":
            table = "primera_comunion";
            fecha = "fecha_p_c";
            break;
        case "3":
            table = "confirmacion";
            fecha = "fecha_c";
            break;
    }

    var conn = DatabaseConnector.getDatabaseConnection();
    var argument_query = "select count(*) as total from " + table + " where MONTH(" + fecha + ") = " + month + " and YEAR(" + fecha + ") = " + anio + "";
    var que = "SELECT COUNT(*) as total from " + table + " WHERE year(" + fecha + ") = " + anio + " and month(fechaBautizo) = " + month + ""
    conn.generateTable(argument_query, (err, doc, fields) => {
        console.log(argument_query);
        res.send(doc);
    });

    conn.close();

});




app.get('/estadistics', myMiddleware, (req, res) => {


    var conn = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '123',
        database: 'sacramentos'
    });



   
    conn.connect(function (err) {


        //Bautizo
        conn.query('SELECT YEAR(fechaBautizo) as anio FROM bautizos', function (err, rows, result) {
            if (err) throw err;
            for (var i = 0; i < rows.length; i++) {
                var id = rows[i].anio;
                var index = years.indexOf(id);

                if (index == "-1") {
                    years.push(id);

                }  
            }
        });

        //Priemra comuncion
        conn.query('SELECT YEAR(fecha_p_c) as anio FROM primera_comunion', function (err, rows, result) {
            if (err) throw err;
            for (var i = 0; i < rows.length; i++) {
                var id = rows[i].anio;
                var index = years.indexOf(id);

                if (index == "-1") {
                    years.push(id);

                }
            }
        });

        //Primera Confirmacion
        conn.query('SELECT YEAR(fecha_c) as anio FROM confirmacion', function (err, rows, result) {
            if (err) throw err;
            for (var i = 0; i < rows.length; i++) {
                var id = rows[i].anio;
                var index = years.indexOf(id);

                if (index == "-1") {
                    years.push(id);

                }
            }
        });

        conn.end;
    });



    res.sendFile(path.join(__dirname, "pages/estadistics.html"));
});

app.get('/cleanCookie', (req,res)=>{

	res.cookie('user', 'ninguno', { maxAge: 30*60*1000 });
	res.redirect("/");
});

app.get('/pdf_file', (req,res)=>{

	res.sendFile(path.join(__dirname,"pages/pdf_created/cedula.pdf"));
});


app.post('/bringYears', parser, (req, res) => {

    anios = ""
    years.forEach(function (element) {
        anios = anios + "<option value = '" + element + "'>" + element + "</option>"
    });

    res.send(anios);


});

app.post('/generate_pdf', parser,(req,res)=>{
	//res.sendFile(path.join(__dirname,"pages/pdf_files/1.pdf"));
	var str_info=req.body.info;
	var sacramento=req.body.tipoSacramento;
	var json_info = JSON.parse(str_info);
	var u_cookie=req.cookies.user;
	var us= new UserDp(u_cookie);
	var pdf_file="";
	if(sacramento==1){
		pdf_file="bautizo_template.html";
	}else if(sacramento==2){
		pdf_file="pc_template.html";
	}else if(sacramento==3){
		pdf_file="pbp_template.html";
	}
	//console.log(str_info);
	fs.readFile(path.join(__dirname,"pages/pdf_template/"+pdf_file),'utf8', (err, data) => {
	  if (err){
	  	throw err;
	  	res.send(err);
	  } 
	  else{
	  		//file=data.toString();
	  		//console.log('file: '+file);
	  		var date = myDate();
	  		var newStr = data;

	  		if(sacramento==1){
				newStr= newStr.replace("#name_#",json_info.nombreB +" "+json_info.apellPatB +" "+json_info.apellMatB);
				newStr= newStr.replace("#name_parroco#",json_info.nombreSacerdote);
				newStr= newStr.replace("#dirNac#",json_info.dirNacimiento);
			}else if(sacramento==2){
				newStr= newStr.replace("#name_parro#",json_info.parroquia_bau);
				newStr= newStr.replace("#name_#",json_info.nombre_com +" "+json_info.apellPat_com +" "+json_info.apellMat_com);
				newStr= newStr.replace("#name_parroco#",json_info.parroco);
				newStr= newStr.replace("#fecha_sacr#",json_info.fecha_p_c);
				newStr= newStr.replace("#fecha_bau#",json_info.fechaBautizo);
				newStr= newStr.replace("#dire_parroquia#",json_info.dirBautizo);
			}else if(sacramento==3){
				newStr= newStr.replace("#name_#",json_info.nombre_con +" "+json_info.apellPat_con +" "+json_info.apellMat_con);
				newStr= newStr.replace("#name_obispo#",json_info.obispo_encargado);
				newStr= newStr.replace("#name_parroco#",json_info.parroco);
				newStr= newStr.replace("#fecha_bau#",json_info.fechaBautizo);
				newStr= newStr.replace("#name_parro_bau#",json_info.parroquiaBautizo);
				newStr= newStr.replace("#dir_nac#",json_info.dirNac);
			}
	  		newStr= newStr.replace("#name_parroquia#",us.getParroquia());
	  		newStr= newStr.replace("#fecha_hoy#",date);
	  		
	  		newStr= newStr.replace("#fec_nac#",myDate(json_info.fechaNac));
	  		newStr= newStr.replace("#padre#",json_info.nombrePaterno);
	  		newStr= newStr.replace("#madre#",json_info.nombreMaterno);
	  		newStr= newStr.replace("#padrino#",json_info.nombrePadrino);
	  		newStr= newStr.replace("#Madrina#",json_info.nombreMadrina);
	  		newStr= newStr.replace("#libro#",json_info.libro);
	  		newStr= newStr.replace("#foja#",json_info.foja);
	  		newStr= newStr.replace("#partida#",json_info.partida);
	  		

	  		var options = { format: 'Letter',
	  							 orientation: "landscape"};
	  		pdf.create(newStr, options).toFile(path.join(__dirname,"pages/pdf_created/cedula.pdf"), function(err, ress) {
			  if (err){
			  		console.log(err);
			  		res.send('0');
			  } 
			  else {
			  	console.log(ress); // { filename: '/app/businesscard.pdf' } 
			  	res.send('1');
			  }
			});
	  }
	});


});

app.post('/searchSacramentos', parser,(req,res)=>{
	var json_tables={1:"bautizos",2:"primera_comunion",3:"confirmacion"};
	var json_date={1:"fechaBautizo",2:"fecha_p_c",3:"fecha_c"};
	var json_bau={1:"nombreB",2:"apellPatB",3:"apellMatB"};
	var json_p_c={1:"nombre_com",2:"apellPat_com",3:"apellMat_com"};
	var json_com={1:"nombre_con",2:"apellPat_con",3:"apellMat_con"};

	var str_info=req.body.info;
	var source=req.body.source;
	var sacramento=req.body.sacramento;
	var json_info = JSON.parse(str_info);
	var table= json_tables[sacramento]
	var row_date= json_date[sacramento]
	var argument_query= "";
	if(source==1){
		argument_query="select * from "+table+" where id="+json_info.id+" ORDER BY STR_TO_DATE("+row_date+", '%Y/%m/%d')";
	}else if (source==2) {
		var j="";
		if(sacramento==1)j=json_bau;
		if(sacramento==2)j=json_p_c;
		if(sacramento==3)j=json_com;
		argument_query="select * from "+table+
							" WHERE "+j[1]+" LIKE '%"+json_info.nombre+"%' OR "+
							j[2]+" LIKE '%"+json_info.ap_p+"%' OR "+
							j[3]+" LIKE '%"+json_info.ap_m+"%'";
	}

	var conn = DatabaseConnector.getDatabaseConnection();
	conn.select('personal',argument_query,(err,doc)=>{
		console.log("err: "+err);
			if(err==null){
				if(typeof doc !== 'undefined'){
					if(doc == null){
						//cursor.close();
						conn.close();
						res.send('0');
					}else{
						if(doc.length>0){
							for (i=0;i<doc.length;i++) {
							console.log("*************");
								for(var name in doc[i]){
									var value= doc[i][name];
									console.log("{"+name+" : "+value+"}");
								}
							}
							var doc_str = JSON.stringify(doc);
							var json_doc = JSON.parse(doc_str);
							//console.log("{"+json_doc[0].id+" : "+json_doc[0].nombre_con+"}");

							conn.close();
							//res.redirect('./administrar');
							res.send(doc_str);
						}else{
							conn.close();
							res.send('0')
						}
						
					}
				}else{
					conn.close();
					res.send('0');
				}
			}else{
				console.log(err.code);
				res.send('0');
			}
	});

});
var server= app.listen(9002, ()=>{
	console.log('New local server for sacramentoapp is ready at port 9002');
});

function myMiddleware(req, res, next) {
	console.log('Cookies: ', req.cookies.user);
	var u_cookie=req.cookies.user;

	if(typeof u_cookie !== 'undefined'){
		if (u_cookie!=='ninguno') {
			next(); 
		}else{
			res.redirect("/");
		}
	}else{
		res.redirect("/");
	}
	 
}

class UserDp {
    constructor(user) {
		var res = user.split("-**-");
		this.id = res[0];
		this.nombre = res[1].replace(/ /g, "_");
		this.apellidoP = res[2].replace(/ /g, "_");
		this.apellidoM = res[3].replace(/ /g, "_");
		this.idParroquia = res[4].replace(/ /g, "_");
		this.parroquia = res[5].replace(/ /g, "_");
		this.titulo = res[6].replace(/ /g, "_");
		this.id_res = res[7];
		this.nom_res = res[8].replace(/ /g, "_");;
    }
    setId(id){
    	this.id=id;
    }
    setNombre(nombre){
    	this.nombre=nombre;
    }
    setApellidoP(apellidoP){
    	this.apellidoP=apellidoP;
    }
    setApellidoM(apellidoM){
    	this.apellidoM=apellidoM;
    }
    setIdParroquia(idParroquia){
    	this.idParroquia=idParroquia;
    }
    setParroquia(parroquia){
    	this.parroquia=parroquia;
    }
    setTitulo(titulo){
    	this.titulo=titulo;
    }
    setIdRes(id_res){
    	this.id_res=id_res;
    }
    setNomRes(nom_res){
    	this.nom_res=nom_res;
    }

    getId(){
    	return this.id;
    }
    getNombre(){
    	return this.nombre.replace(/_/g, " ");
    }
    getApellidoP(){
    	return this.apellidoP.replace(/_/g, " ");
    }
    getApellidoM(){
    	return this.apellidoM.replace(/_/g, " ");
    }
    getIdParroquia(){
    	return this.idParroquia;
    }
    getParroquia(){
    	return this.parroquia.replace(/_/g, " ");
    }
    getTitulo(){
    	return this.titulo.replace(/_/g, " ");
    }
    getIdRes(){
    	return this.id_res;
    }
    getNomRes(){
    	return this.nom_res.replace(/_/g, " ");
    }
    toString() {
      return this.id+"-**-"+this.nombre+"-**-"+this.apellidoP+"-**-"+this.apellidoM+"-**-"+this.idParroquia+"-**-"+this.parroquia+"-**-"+this.titulo+"-**-"+this.id_res+"-**-"+this.nom_res;
    }
    toStringNoSpace() {
    	var n_n=this.nombre.replace(/_/g," ");
    	var n_p=this.apellidoP.replace(/_/g," ");
    	var n_m=this.apellidoM.replace(/_/g," ");
    	var n_name_parr=this.parroquia.replace(/_/g," ");
    	var n_name_titulo=this.titulo.replace(/_/g," ");
    	var n_nam_r=this.nom_res.replace(/_/g," ");
      return this.id+"-**-"+n_n+"-**-"+n_p+"-**-"+n_m+"-**-"+this.idParroquia+"-**-"+n_name_parr+"-**-"+n_name_titulo+"-**-"+this.id_res+"-**-"+this.nom_res;
    }

}



function myDate(fecha=null){
	var meses={0:"Enero",1:"Febrero",2:"Marzo",3:"Abril",
				4:"Mayo",5:"Junio",6:"Julio",7:"Agosto",8:"Septiembre",
				9:"Octubre",10:"Noviembre",11:"Diciembre"};
	//var meses= ['Enero', 'Febrero']
	var dd=null,mm=null,yyyy=null;
	if(fecha!=null){
		var res = fecha.split("-");
		dd=res[2][0]+""+res[2][1];
		mm=res[1];
		yyyy=res[0];
	}
	var today = new Date();
	if (dd==null) {
		dd = today.getDate();
	}

	if (mm==null) {
		mm = today.getMonth(); //January is 0!
	}else{
		mm=parseInt(mm)-1;
	}

	if (yyyy==null) {
		yyyy = today.getFullYear();
	}
	
	 	
	 month= meses[mm];


	var today = dd+' de '+month+' del '+yyyy;
	return today;
}


