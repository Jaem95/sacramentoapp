const mysql = require('mysql');

class DatabaseController{
    
    
    constructor(){
        console.log("connecting");
        this.connection = mysql.createConnection({
			  host     : 'localhost',
			  user     : 'root',
			  password : '123',
			  database : 'sacramentos'
			});
        //this.db=null;

    }
    
    static getDatabaseConnection(){
        
        if( DatabaseController.databaseInstance == null){
            
            DatabaseController.databaseInstance = new DatabaseController();
        
        }
        
        return DatabaseController.databaseInstance; 
    }
    
    select(table,argument,cb){
    	//console.log("making select");
        var my_query=argument;
        console.log("query: "+my_query);
    	if( this.connection != null){

    		this.connection.connect();

			this.connection.query(my_query, function(err, rows, fields) {
            cb(err,rows);
			});

			//this.connection.end();
    	}else{
    		this.connection = mysql.createConnection({
            host     : 'localhost',
            user     : 'root',
            password : '123',
            database : 'sacramentos'
			});

    		this.connection.connect();

			this.connection.query(my_query, function(err, rows, fields) {
			  cb(err,rows);
			});
			
    	}
    }

    insert(table,argument,data,cb){
        //console.log("making select");
        //INSERT INTO diocesis (nombre) VALUES('Toluca');


        var my_query="INSERT INTO "+table+" "+argument+"";
        //console.log("--------------data: \n"+data);
        if( this.connection != null){

            this.connection.connect();
            
            var query=this.connection.query(my_query,data, function(err, results) {
                my_query="";
                cb(err,results);
            });
            //console.log(query.sql);
            //this.connection.end();
        }else{
            this.connection = mysql.createConnection({
            host     : 'localhost',
            user     : 'root',
            password : '123',
            database : 'sacramentos'
            });

            this.connection.connect();

            this.connection.query(my_query,data, function(err, results, fields) {
                cb(err,results);
            });
            
        }
    }

generateTable(my_query, cb) {
    var numRows;
    console.log(my_query);

    if (this.connection != null) {

        
        this.connection.connect();

        this.connection.query(my_query, function (err, rows, fields) {
            cb(err, rows,fields);
        });

			            //this.connection.end();
} else {

this.connection = mysql.createConnection({

    host     : 'localhost',

    user     : 'root',

    password : '123',

    database : 'sacramentos'

});

        
        this.connection.connect();

        this.connection.query(my_query, function (err, rows, fields) {
            cb(err, rows, fields);
        });


			
}

    
    
			
}

   
   

    close(){
        
        if( this.connection != null)
            this.connection.end();
        this.connection= null;
    }
}


DatabaseController.databaseInstance = null;

module.exports = DatabaseController;