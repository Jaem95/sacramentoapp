function checkInfoLogin(){
	var user = document.getElementById('user_mail_index').value;
	var password= document.getElementById('user_pass_index').value;
	var mssg_span =document.getElementById('index_err_mss');

	if(user==''||password==""){
		mssg_span.innerHTML='Rellena los 2 campos por favor';
		mssg_span.style.display='inline';
	}else{
		mssg_span.style.display='none';

		login(user,password);
	}	
}

function login(user,password){
	//alerta(user+"//"+password);
	var mssg_span =document.getElementById('index_err_mss');
	$.ajax({
      type: "POST",
      url: "/try_login",
      data: {user:user,pass:password},
      success: function(data) 
      {
         //window.open("downloaded_files/"+data, "_blank");
         //document.getElementById('my_iframe').src = data;
         //alerta(data);
         if(data!=1){
         	mssg_span.innerHTML='Usuario o contraseña incorrectos';
         	mssg_span.style.display='inline';
         	console.log("error en login");
         }else{
         	console.log("acierto en login");
         	window.open("/administrar","_self")
         	//console.log("acierto en login");
         }
      },
      error: function() 
      {
         alerta("Error");
      }
   });
}


function checkInfoBautizo(){
	//alerta("checando info");
	var nom_b = document.getElementById('nom_bau').value;
	var ap_b = document.getElementById('ap_bau').value;
	var am_b = document.getElementById('am_bau').value;
	var foja_b = document.getElementById('foja_bau').value;
	var libro_b = document.getElementById('libro_bau').value;
	var partida_b = document.getElementById('partida_bau').value;
	var f_n_b = document.getElementById('fecha_nac_bau').value;
	var f_s_b = document.getElementById('fecha_sac_bau').value;
	var d_n_b = document.getElementById('dir_nac_bau').value;
	var n_sac_b = document.getElementById('nom_sac_bau').value;
	var n_p_b = document.getElementById('nom_p_bau').value;
	var n_m_b = document.getElementById('nom_m_bau').value;
	var n_padri_b = document.getElementById('nom_padr_bau').value;
	var n_madri_b = document.getElementById('nom_madr_bau').value;

	if(nom_b==""){
		alerta("Por Favor llena el nombre del bautizado");

	}else if(ap_b==""){
		alerta("Por Favor llena el apellido paterno del bautizado");

	}else if(am_b==""){
		alerta("Por Favor llena apellido materno del bautizado");
		
	}else if(foja_b==""){
		alerta("Por Favor llena la foja");
		
	}else if(libro_b==""){
		alerta("Por Favor llena el libro");
		
	}else if(partida_b==""){
		alerta("Por Favor llena la partida");
		
	}else if(f_n_b==""){
		alerta("Por Favor llena la fecha de nacimiento del bautizado");
		
	}else if(f_s_b==""){
		alerta("Por Favor llena la fecha del sacramento");
		
	}else if(d_n_b==""){
		alerta("Por Favor llena la dirección del nacimiento del bautizado");
		
	}else if(n_sac_b==""){
		alerta("Por Favor llena el nombre del sacerdote");
		
	}else if(n_p_b==""){
		alerta("Por Favor llena el nombre del padre del bautizado");
		
	}else if(n_m_b==""){
		alerta("Por Favor llena el nombre de la madre del bautizado");
		
	}else if(n_padri_b==""){
		alerta("Por Favor llena el nombre del padrino del bautizado");
		
	}else if(n_madri_b==""){
		alerta("Por Favor llena el nombre de la madrina del bautizado");
		
	}else{
		var json_bau={foja:foja_b ,
							libro:libro_b ,
							partida:partida_b,
							nombreB:nom_b,
							apellPatB:ap_b,
							apellMatB:am_b,
							parroquia:"",
							dirNacimiento:d_n_b,
							fechaBautizo:f_s_b ,
							nombreSacerdote:n_sac_b,
							fechaNac: f_n_b,
							nombrePaterno:n_p_b,
							nombreMaterno:n_m_b,
							nombrePadrino:n_padri_b,
							nombreMadrina:n_madri_b,
							registradoPor:""};
		insertSacramento(json_bau,1);
	}
}

function checkInfoPrimeraComunion(){
	//alerta("checando info pc");
	var nom_b = document.getElementById('nom_pc').value;
	var ap_b = document.getElementById('ap_pc').value;
	var am_b = document.getElementById('am_pc').value;
	var foja_b = document.getElementById('foja_pc').value;
	var libro_b = document.getElementById('libro_pc').value;
	var partida_b = document.getElementById('partida_pc').value;
	//console.log("1111");
	var f_s_b = document.getElementById('fecha_sac_bau').value;
	var n_s_pc = document.getElementById('nom_sac_pc').value;
	var n_p_pc = document.getElementById('nom_p_pc').value;
	var n_m_pc = document.getElementById('nom_m_pc').value;
	//console.log("2222");
	var n_pr_pc = document.getElementById('nom_padr_pc').value;
	var n_mr_pc = document.getElementById('nom_madr_pc').value;
	var dirBautizo= document.getElementById('dirBautizo').value;
	var fechaBautizo= document.getElementById('fechaBautizo').value;
	var parroquia_bau=document.getElementById('parroquia_bau').value;
	if(nom_b==""){
		alerta("Por Favor llena el nombre ");

	}else if(ap_b==""){
		alerta("Por Favor llena el apellido paterno");

	}else if(am_b==""){
		alerta("Por Favor llena apellido materno");
		
	}else if(foja_b==""){
		alerta("Por Favor llena la foja");
		
	}else if(libro_b==""){
		alerta("Por Favor llena el libro");
		
	}else if(partida_b==""){
		alerta("Por Favor llena la partida");
		
	}else if(f_s_b==""){
		alerta("Por Favor llena la fecha del sacramento");
		
	}else if(n_s_pc==""){
		alerta("Por Favor llena el nombre del sacerdote");
		
	}else if(n_p_pc==""){
		alerta("Por Favor llena el nombre del padre");
		
	}else if(n_m_pc==""){
		alerta("Por Favor llena el nombre de la madre");
		
	}else if(n_pr_pc==""){
		alerta("Por Favor llena el nombre del padrino");
		
	}else if(dirBautizo==""){
		alerta("Por Favor llena la dirección del bautizo");
		
	}else if(fechaBautizo==""){
		alerta("Por Favor llena la fecha del bautizo");
		
	}else if(n_mr_pc==""){
		alerta("Por Favor llena el nombre de la madrina");
		
	}else if(parroquia_bau==""){
		alerta("Por Favor llena el nombre de la parroquia del bautizo");
		
	}else{
		var json_bau={nombre_com:nom_b,
							apellPat_com:ap_b,
							apellMat_com:am_b,
							foja:foja_b,
							libro:libro_b,
							partida:partida_b,
							fecha_p_c:f_s_b,
							parroco:n_s_pc,
							nombrePaterno:n_p_pc,
							nombreMaterno:n_m_pc,
							nombrePadrino:n_pr_pc,
							nombreMadrina:n_mr_pc,
							dirBautizo:dirBautizo,
							fechaBautizo:fechaBautizo,
							parroquia_bau:parroquia_bau,
							parroquia:"",
							registradoPor:""};
		insertSacramento(json_bau,2);
	}
}


function checkInfoConfirmacion(){
	alerta("checando info confirmacion");

	var nom_com = document.getElementById('nom_com').value;
	var ap_com = document.getElementById('ap_com').value;
	var am_com = document.getElementById('am_com').value;
	var foja_com = document.getElementById('foja_com').value;
	var libro_com = document.getElementById('libro_com').value;
	var partida_com = document.getElementById('partida_com').value;
	var fecha_sac_com = document.getElementById('fecha_sac_com').value;
	var nom_sac_com = document.getElementById('nom_sac_com').value;
	var nacimi_com = document.getElementById('nacimi_com').value;
	var naci_fec_com = document.getElementById('naci_fec_com').value; 
	var parr_bau_com = document.getElementById('parr_bau_com').value;
	var fec_bau_com = document.getElementById('fec_bau_com').value;
	var foja_bau_com = document.getElementById('foja_bau_com').value;
	var libro_bau_com = document.getElementById('libro_bau_com').value;
	var acta_bau_com = document.getElementById('acta_bau_com').value;
	var nom_p_com = document.getElementById('nom_p_com').value;
	var nom_m_com = document.getElementById('nom_m_com').value;
	var nom_padr_com = document.getElementById('nom_padr_com').value;
	var nom_madr_com = document.getElementById('nom_madr_com').value;
	var partida_bau_com = document.getElementById('partida_bau_com').value;
	var dirBautizo = document.getElementById('dirBautizo').value;
	var nom_obispo=document.getElementById('nom_obispo').value

	if(nom_com==""){
		alerta("Por Favor llena el nombre ");

	}else if(ap_com==""){
		alerta("Por Favor llena el apellido paterno");

	}else if(am_com==""){
		alerta("Por Favor llena apellido materno");
		
	}else if(foja_com==""){
		alerta("Por Favor llena la foja");
		
	}else if(libro_com==""){
		alerta("Por Favor llena el libro");
		
	}else if(partida_com==""){
		alerta("Por Favor llena la partida");
		
	}else if(fecha_sac_com==""){
		alerta("Por Favor llena la fecha de la confirmación");
		
	}else if(nom_sac_com==""){
		alerta("Por Favor llena el nombre del sacerdote");
		
	}else if(nacimi_com==""){
		alerta("Por Favor llena la dirección del nacimiento");
		
	}else if(naci_fec_com==""){
		alerta("Por Favor llena la fecha de nacimiento");
		
	}else if(parr_bau_com==""){
		alerta("Por Favor llena el nombre de la parroquia");
		
	}else if(fec_bau_com==""){
		alerta("Por Favor llena la fecha del bautizo");
		
	}else if(foja_bau_com==""){
		alerta("Por Favor llena la foja del bautizo");
		
	}else if(libro_bau_com==""){
		alerta("Por Favor llena el libro del bautizo");
		
	}else if(partida_bau_com==""){
		alerta("Por Favor llena la partida del bautizo");
	}else if(nom_p_com==""){
		alerta("Por Favor llena el nombre del padre");
		
	}else if(nom_m_com==""){
		alerta("Por Favor llena el nombre de la madre");
		
	}else if(nom_padr_com==""){
		alerta("Por Favor llena el nombre del padrino");
		
	}else if(nom_madr_com==""){
		alerta("Por Favor llena el nombre de la madrina");
	}else if(dirBautizo==""){
		alerta("Por Favor llena la dirección del bautizo");
	}else if(nom_obispo==""){
		alerta("Por Favor llena el nombre del obispo");
	}else if(acta_bau_com==""){
		alerta("Numero del acta esta vacia");
		
	}else{
		var json_bau={nombre_con:nom_com,
							apellPat_con:ap_com,
							apellMat_con:am_com,
							foja:foja_com,
							libro:libro_com,
							partida:partida_com,
							fecha_c:fecha_sac_com,
							parroco:nom_sac_com,
							dirNac:nacimi_com,
							fechaNac:naci_fec_com,
							parroquiaBautizo:parr_bau_com,
							dirBautizo:dirBautizo,
							fechaBautizo:fec_bau_com,
							foja_bautizo:foja_bau_com,
							libro_bautizo:libro_bau_com,
							partida_bautizo:partida_bau_com,
							id_bautizo:acta_bau_com,
							nombrePaterno:nom_p_com,
							nombreMaterno:nom_m_com,
							nombrePadrino:nom_padr_com,
							nombreMadrina:nom_madr_com,
							parroquia:"",
							obispo_encargado:nom_obispo,
							registradoPor:""};
		insertSacramento(json_bau,3);
	}
}

var my_win="";
var my_win2="";
function insertSacramento(json_bau,tipoSacramento){
	//console.log(json_bau);
	var j=JSON.stringify(json_bau);
	my_win= window.open('','_blank');
	my_win2= window.open('','_self');
	$.ajax({
      type: "POST",
      url: "/insertSacramento",
      data: {info:j,sacramento:tipoSacramento},
      success: function(data) 
      {
         if(data!=1){
         	if(data==3){
         		alerta("Fecha insertada con formato incorrecto, por favor revisar");
         	}else{
         		alerta("Error, carga de nuevo la página");
         	}
         	
         }else{
         	
         	generatePdf(j,tipoSacramento);
         	
         	console.log("consola");
         }
      },
      error: function() 
      {
         alerta("Error");
      }
   });
}

function generatePdf(json_bau,tipoSacramento){
	
	console.log("jason: "+json_bau);
	$.ajax({
      type: "POST",
      url: "/generate_pdf",
      data: {info:json_bau, tipoSacramento:tipoSacramento},
      success: function(data) 
      {
         //console.log(data);
         if(data==1){
         	my_win.location.href='http://localhost:9002/pdf_file';
         	my_win2.location.href='http://localhost:9002/editarIniciacion';
         }else{
         	alerta("Error mientras se generaba la cedula, intentalo otra vez por favor")
         }
         
      },
      error: function() 
      {
         alerta("Error");
      }
   });
}

function checkInfoSearch(source){
	var div_result=document.getElementById('result_search');
	var img_load=document.getElementById('img_load').style.display = "none";
	var sacramento=document.getElementById('sacramento2').value;
	div_result.innerHTML="";
	if(sacramento!=""){
		if (source==1) {
			var id_ced_search = document.getElementById('id_ced_search').value;
			if (id_ced_search=="") {
				//var message="<h6 style='color:red;'>"+
				//				"Por favor llena el campo de Id"+
				//				"</h6>";
				//div_result.innerHTML=message;
                $(function () {
                    $('#myModal').modal('toggle');
                });
                alerta("Por favor llena el campo de Acta");
			}else{
				img_load=document.getElementById('img_load').style.display = "inline";
				var info={
					id:id_ced_search
				}
				buscarSacramentos(info,source,sacramento);
			}
		}

	if (source==2) {
		var nom_search = document.getElementById('nom_search').value;
		var ap_pat_search = document.getElementById('ap_pat_search').value;
		var ap_mat_search = document.getElementById('ap_mat_search').value;
		if (nom_search == "" || ap_pat_search == "" || ap_mat_search == "") {
			//var message="<h6 style='color:red;'>"+
			//				"Por favor llena todos los campos en busqueda por nombre"+
			//				"</h6>";
			//div_result.innerHTML=message; 
                $(function () {
                    $('#myModal').modal('toggle');
                });
                alerta("Por favor llena todos los campos en busqueda por nombre");
		}else{
			img_load=document.getElementById('img_load').style.display = "inline";
			var info={
				nombre:nom_search,
				ap_p:ap_pat_search,
				ap_m:ap_mat_search
			}
			buscarSacramentos(info,source,sacramento);
		}

	}
    } else {
        $(function () {
            $('#myModal').modal('toggle');
        });
        alerta("Primero selecciona un sacramento");
		//var message="<h6 style='color:red;'>"+
		//					"Primero selecciona un sacramento en la sección de arriba"+
		//					"</h6>";
		//	div_result.innerHTML=message;
       
	}

}
var my_win3="";
function buscarSacramentos(info,source,sacramento){
	info=JSON.stringify(info);
	console.log(info+"//"+source+"//"+sacramento);
	
	$.ajax({
      type: "POST",
      url: "/searchSacramentos",
      data: {info:info, source:source,sacramento:sacramento},
      success: function(data) 
      {

      	var div_result=document.getElementById('result_search');
         if (data==0) {
    //     	document.getElementById('img_load').style.display = "none";
    //     	var message="<h6 style='color:red;'>"+
				//			"No se encontraron resultados con esos parametros"+
				//			"</h6>";
				//div_result.innerHTML=message;
                $(function () {
                    $('#myModal').modal('toggle');
                });
                alerta("No se encontraron resultados con esos parametros");
		//var message="<h6 style='color:red;'>"+
         }else{
         	var json_doc = JSON.parse(data);
         	var pos_sa=9
         	if(sacramento==3){
         		pos_sa=8;
         	}

         	var mss=generateTable(json_doc,pos_sa,sacramento);
				console.log("um: "+mss);
				div_result.innerHTML=mss;
				document.getElementById('img_load').style.display = "none";

         }
      },
      error: function() 
      {
         alerta("Error");
      }
   });
}

var array_json_users=[];

function generateTable(arr,pos_sa,sacramento){
	var json_date={1:"fechaBautizo",2:"fecha_p_c",3:"fecha_c"};
	var mss="<table class='table'>"+
         			"<thead>"+"<th>No. Acta</th>"+
         						"<th>Nombre</th>"+
         						"<th>Apellido Paterno</th>"+
         						"<th>Apellido Materno</th>"+
         						"<th>Fecha Sacramento</th>"+
         			"</thead>";
   var roe_date=json_date[sacramento];

		for (i=0;i<arr.length;i++) {
		mss=mss+"<tr>";
		var j=0;
			for(var name in arr[i]){
				var value= arr[i][name];
				if(name==roe_date){
					today=new Date(value);
					var dd = today.getDate();
					var mm = today.getMonth()+1	;
					var yyyy = today.getFullYear();
					value=yyyy+"-"+mm+"-"+dd;
					mss=mss+"<td>"+value+"</td>";
				}
				if (j==0) {
					mss=mss+"<td>"+value+"</td>";
				}
				if(j>=4 && j<=6){
					mss=mss+"<td>"+value+"</td>";
				}
				
				j++;
			}
			var valueToPush = { };
			valueToPush.id = arr[i].id;
			valueToPush.info_json = arr[i];
			array_json_users.push(valueToPush);

		mss=mss+"<td><button class='btn  btn-info' onClick='(generatePdf2("+sacramento+","+arr[i].id+"))'><i class='fa fa-file-pdf-o' aria-hidden='true'></i>&nbsp;PDF</button></td></tr>";
		}
	return mss+"</table>";
	
}

function generatePdf2(sacramento,id){
	my_win3= window.open('','_blank');
	console.log(sacramento+"--"+id);
	var json_bau;
	//alerta("json");
		for (i=0;i<array_json_users.length;i++) {
			if(array_json_users[i].id==id){
				json_bau = array_json_users[i].info_json;
				//console.log(json_bau+"--");
			}
		}
	var j=JSON.stringify(json_bau);
	console.log(j+"*****");
	$.ajax({
      type: "POST",
      url: "/generate_pdf",
      data: {info:j, tipoSacramento:sacramento},
      success: function(data) 
      {
         //console.log(data);
         if(data==1){
         	my_win3.location.href='http://localhost:9002/pdf_file';
         	//my_win2.location.href='http://localhost:9002/editarIniciacion';
         }else{
         	alerta("Error mientras se generaba la cedula, intentalo otra vez por favor")
         }
         
      },
      error: function() 
      {
         alerta("Error");
      }
   });

	
}

function alerta(msg) {
    swal({
        title: '¡Error!',
        text: msg,
        type: 'warning',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'OK'
    }).then(function () {
       
    });
}
