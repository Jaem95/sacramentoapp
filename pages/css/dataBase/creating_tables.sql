ALTER TABLE parroquia DROP FOREIGN KEY responsable_to_pID;
ALTER TABLE personal DROP FOREIGN KEY register_to_pID;

drop table personal;
drop table parroquia;
drop table diocesis;
drop table tipoDeTitulos;


create table tipoDeTitulos(
	id INT NOT NULL AUTO_INCREMENT ,
	nombre varchar(200) NOT NULL,
	PRIMARY KEY (id)
);
INSERT INTO tipoDeTitulos (nombre) VALUES('Encargado de la parroquia');

create table diocesis(
	id INT NOT NULL AUTO_INCREMENT ,
	nombre varchar(200) NOT NULL,
	PRIMARY KEY (id)
);
INSERT INTO diocesis (nombre) VALUES('Toluca');

create table parroquia(
	id INT NOT NULL AUTO_INCREMENT,
	direccion varchar(200) NOT NULL,
	nombre varchar(200) NOT NULL,
	municipio varchar(200) NOT NULL,
	responsable int not null,
	diocesis int NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (diocesis) REFERENCES diocesis(id)
);
INSERT INTO parroquia VALUES(0,'direccion de la parroquia',
									  'Parroquia de San Miguel Arcángel Ameyalco',
									  'Toluca',
									  1,
									  1);

create table personal(
	id INT NOT NULL AUTO_INCREMENT,
	correo varchar(200) NOT NULL,
	contrasena varchar(200) NOT NULL,
	nombre varchar(200) NOT NULL,
	apellidoP varchar(200) NOT NULL,
	apellidoM varchar(200) NOT NULL,
	fechaNac date NOT NULL,
	estatus int not null,
	parroquia int not null,
	titulo int not null,
	registradoPor int not null,
	PRIMARY KEY (id),
	FOREIGN KEY (parroquia) REFERENCES parroquia(id),
	FOREIGN KEY (titulo) REFERENCES tipoDeTitulos(id)
);

INSERT INTO personal VALUES(0,'admin@correo.com',
										'admin_pass',
									  'admin',
									  'admin paterno',
									  'admin materno',
									  '1995-08-28',
									  1,
									  1,
									  1,
									  1);

ALTER TABLE parroquia ADD CONSTRAINT responsable_to_pID FOREIGN KEY (responsable) REFERENCES personal(id);
ALTER TABLE personal ADD CONSTRAINT register_to_pID FOREIGN KEY (registradoPor) REFERENCES personal(id);

/*
CREATE TABLE empleado(
	nombre varchar(20) not null,
	direccion varchar(20) not null,
	sexo varchar(1) not null,
	salario float not null,
	fecha_nac date not null,
	nsup varchar(6) not null,
	ndep varchar(6) not null,
	PRIMARY KEY (nss), 
	FOREIGN KEY (ndep) REFERENCES departamento(ndepto),
	FOREIGN KEY (nsup) REFERENCES departamento(nss)
);*/