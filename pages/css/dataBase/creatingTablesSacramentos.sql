drop table bautizos;
drop table primera_comunion;
drop table confirmacion;

create table bautizos(
	id INT NOT NULL AUTO_INCREMENT,
	foja int not null,
	libro int not null,
	partida int not null,
	nombreB varchar(200) NOT NULL,
	apellPatB varchar(200) NOT NULL,
	apellMatB varchar(200) NOT NULL,
	parroquia int NOT NULL,
	dirNacimiento varchar(200) NOT NULL,
	fechaBautizo date NOT NULL,
	nombreSacerdote varchar(200) NOT NULL,
	fechaNac date NOT NULL,
	nombrePaterno varchar(200) NOT NULL,
	nombreMaterno varchar(200) NOT NULL,
	nombrePadrino varchar(200) NOT NULL,
	nombreMadrina varchar(200) NOT NULL,
	registradoPor int not null,
	PRIMARY KEY (id),
	FOREIGN KEY (parroquia) REFERENCES parroquia(id)
);

create table primera_comunion(
	id INT NOT NULL AUTO_INCREMENT,
	foja int not null,
	libro int not null,
	partida int not null,
	nombre_com varchar(200) NOT NULL,
	apellPat_com varchar(200) NOT NULL,
	apellMat_com varchar(200) NOT NULL,
	parroquia int NOT NULL,
	parroco varchar(200) NOT NULL,
	fecha_p_c date NOT NULL,
	nombrePaterno varchar(200) NOT NULL,
	nombreMaterno varchar(200) NOT NULL,
	nombrePadrino varchar(200) NOT NULL,
	nombreMadrina varchar(200) NOT NULL,
	registradoPor int not null,
	PRIMARY KEY (id),
	FOREIGN KEY (parroquia) REFERENCES parroquia(id)
);
create table confirmacion(
	id INT NOT NULL AUTO_INCREMENT,
	foja int not null,
	libro int not null,
	partida int not null,
	nombre_con varchar(200) NOT NULL,
	apellPat_con varchar(200) NOT NULL,
	apellMat_con varchar(200) NOT NULL,
	parroquia int NOT NULL,
	fecha_c date NOT NULL,
	parroco varchar(200) NOT NULL,
	obispo_encargado varchar(200) NOT NULL,
	dirNac varchar(200) NOT NULL,
	dirBautizo varchar(200) NOT NULL,
	fechaNac date NOT NULL,
	parroquiaBautizo varchar(200) NOT NULL,
	fechaBautizo date NOT NULL,
	foja_bautizo int not null,
	libro_bautizo int not null,
	partida_bautizo int not null,
	id_bautizo int,
	nombrePaterno varchar(200) NOT NULL,
	nombreMaterno varchar(200) NOT NULL,
	nombrePadrino varchar(200) NOT NULL,
	nombreMadrina varchar(200) NOT NULL,
	registradoPor int not null,
	PRIMARY KEY (id),
	FOREIGN KEY (parroquia) REFERENCES parroquia(id)
);

