const express           = require('express');
const router            = express.Router();
var path 					= require("path");
const DatabaseConnector = require('../lib/DatabaseController');

router.get('/', (req,res)=>{
	//res.sendFile("../pages/index.html");
	res.sendFile(path.join(__dirname,"../index.html"));
});

router.post('/pdf_file', (req,res)=>{
	res.sendFile(path.join(__dirname,"../pages/pdf_files/1.pdf"));
});

module.exports = router;